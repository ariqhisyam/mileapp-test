# Use Alpine Linux 3.7
FROM alpine:3.7

# Install PHP 7.2 
RUN apk --update add php7 php7-fpm php7-opcache php7-json php7-session php7-mbstring php7-xml php7-ctype php7-zip php7-tokenizer php7-xmlwriter php7-xmlreader php7-pdo php7-pdo_mysql php7-json php7-curl php7-dom php7-openssl php7-mysqli php7-bcmath php7-gd

# Install Nginx
RUN apk --no-cache add nginx

# Create the directory for Nginx PID file
RUN mkdir -p /run/nginx

# Set the working directory
WORKDIR /var/www/html

# Copy the Laravel application files to the container
COPY . .

# Configure Nginx
COPY nginx.conf /etc/nginx/nginx.conf

# Expose port 5757
EXPOSE 5757

# Start PHP-FPM and Nginx
CMD ["sh", "-c", "php-fpm7 && nginx -g 'daemon off;'"]
